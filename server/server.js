var express = require('express');
var http = require('http');
var path = require('path');
var fs = require("fs");
var cors = require('cors');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 5000;

// mongoose.connect('mongodb://localhost/test');
mongoose.connect('mongodb://USER:DOMAIN:55754/DB');

app.use(express.static(__dirname + '/../dist/sklep'));


app.use(cors())
app.use(bodyParser());
app.use(bodyParser.json({limit:'5mb'}));
app.use(bodyParser.urlencoded({extended:true}));



var db = mongoose.connection;
db.on('error', console.error.bind(console, 'błąd połączenia...'));
db.once('open', function() {
  // połączenie udane!
  console.log('DB is running...')
});

app.get('/', (req, res) => {
  console.log(__dirname);
  res.sendFile(path.join(__dirname + '/../'))
});

const server = http.createServer(app);

var Schema = mongoose.Schema;

  var Products = new Schema({
    // id: Number,
    name: String,
    category: String,
    qty: Number,
    unit_price: Number,
    description: String,
    url: String
  })

  var Users = new Schema({
    email: String,
    account_type: Number,
    name: String,
    surname: String,
    address: String
  })

  var Orders = new Schema({
    client_name: String,
    client_address: String,
    ordered_products: [{
      product: Products,
      qty: Number
    }],
    order_value: Number,
    products_qty: Number,
    products_collected: [String],
    status: String,
    data: Date,
    u_email: String
  })


  mongoose.model('Product', Products);
  mongoose.model('Users', Users);
  mongoose.model('Order', Orders);

  var Products = mongoose.model('Product');
  var Users = mongoose.model('Users');
  var Orders = mongoose.model('Order');

server.listen(port, () => console.log('Running...'));

app.post('/api/products', function (req, res) {
  let product = new Products({
    name: req.body.name,
    category: req.body.category,
    unit_price: req.body.unit_price,
    qty: req.body.qty,
    description: req.body.description,
    url: req.body.url
  });

  product.save();

  res.status(201).send(product)
})

app.delete('/api/products/:productID', function (req, res) {
  Products.findById(req.params.productID, (err, product) => {
    product.remove(err => {
        if(err){
            res.status(500).send(err)
        }
        else{
            console.log('Product removed!');
            res.status(204).send('removed!')
        }
    })
})
})

app.put('/api/products/:productID', function (req, res) {
  Products.findOneAndUpdate({'_id': req.body._id},
    {
      name: req.body.name,
      category: req.body.category,
      qty: req.body.qty,
      unit_price: req.body.unit_price,
      description: req.body.description,
      url: req.body.url
    },
    {new: true},
    (err,data) => {
    if (err) {
    res.send(err);
    }
    else{
        res.send(data);
      }
  });
})

app.get('/api/products', function (req, res) {
  Products.find({}, function(err, products) {
    var ProductMap = [];

    products.forEach(function(product) {
      ProductMap.push(product);
    });

    res.send(ProductMap);
  });
})

app.get('/api/products/:productID', function (req, res) {
  console.log('Wyszukano produkt');
  Products.findOne({'_id': req.params.productID}, function(err, product) {

    res.send(product);
  });
})


app.post('/api/check_user', function (req, res) {
  Users.findOne({'email': req.body.email}, function(err, user) {
    if (err) {
      res.send(err);
    } else {
      res.send(user);
    }

  });

})

app.post('/api/register_user', function (req, res) {
  console.log("Dodanie użyszkodnika");
  let user = new Users({
    email: req.body.email,
    account_type: req.body.account_type,
    name: req.body.name,
    surname: req.body.surname,
    address: req.body.address
  });

  user.save();

  res.status(201).send(user)
})

app.get('/api/orders', function (req, res) {
  Orders.find({}, function(err, orders) {
    var OrdersMap = [];

    orders.forEach(function(order) {
      // console.log(product);

      OrdersMap.push(order);
    });

    res.send(OrdersMap);
  });
})

app.post('/api/orders', function (req, res) {
  console.log(req.body);
  // console.log("Otrzymano żądanie POST dla strony głównej");
  let order = new Orders({
    client_name: req.body.client_name,
    client_address: req.body.client_address,
    // ordered_products: req.body.ordered_products,
    order_value: req.body.order_value,
    products_collected: req.body.products_collected,
    status: req.body.status
  });

  req.body.ordered_products.forEach (ord_product => {
    order.ordered_products.push(ord_product);
  });

  if (req.body.email) {
    order.email = req.body.email;
  }

  order.save();

  res.status(201).send(order)

})

app.put('/api/orders/:orderID', function (req, res) {
  console.log('update zamowienia');
  Orders.findOneAndUpdate({'_id': req.params.orderID},
    {
      client_name: req.body.client_name,
      client_address: req.body.client_address,
      order_value: req.body.order_value,
      products_collected: req.body.products_collected,
      status: req.body.status
    },
    {new: true},
    (err,data) => {
    if (err) {
    res.send(err);
    }
    else{
        res.send(data);
      }
  });
})

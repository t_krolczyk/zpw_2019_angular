var mongoose = require('mongoose');

// const app = express();
// const port = process.env.PORT || 5000;

// mongoose.connect('mongodb://localhost/test');
mongoose.connect('mongodb://USER:DOMAIN:55754/DB');

var Schema = mongoose.Schema;

var Products = new Schema({
  id: Number,
  name: String,
  category: ['Dania główne', 'Pizza', 'Burgery', 'Makarony'],
  qty: Number,
  unit_price: Number,
  description: String,
  url: String,
})

var Users = new Schema({
  email: String,
  account_type: Number
})

mongoose.model('Product', Products);
mongoose.model('Users', Users);

var Product = mongoose.model('Product');
var Users = mongoose.model('Users');

var product = new Product();
var user = new Users();

product.name = 'kaczka';
product.category = 'Dania główne';
product.qty = 12;
product.unit_price = 26.99;
product.description = 'Pyszna, soczysta kaczka';
product.url = '/assets/images/kaczka.jpg';

user.email = 'tom.krolczyk@gmail.com';
user.account_type = 3;

user.save(function(err) {
    if (err) throw err;
    console.log('User has been saved.');
  });

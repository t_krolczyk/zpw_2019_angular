# Sklep

**Pierwsze zderzenie z technologiami webowymi - projekt zaliczeniowy z przedmiotu Zaawansowane Technologie Webowe**  
**Frontend:** framework Angular + bootstrap + css3  
**Backend:** Node.js, Express, Mongoose  
**Serwer autentykacji:** Usługa Firebase  
**Serwer bazy danych:** mlab.com  
  
Z plików konfiguracyjnych usunięte zostały dane logowania do usług zewnętrznych.

------------------------------------------------------

## Uruchomienie aplikacji w trybie deweloperskim  
1. Aktualizujemy biblioteki zewnętrzne z wykorzystaniem narzędzia npm:  
	npm update  
2. Uzupełniamy dane niezbędne do połączenia z serwisami zewnętrznymi:  
  * firebase:  
   - src/environments/environment.prod.ts - nie jest konieczne do uruchomienia serwera deweloperskiego  
   - src/environments/environment.ts  
  * mlab:  
   - server/server.js  
3. Uruchamiamy serwer deweloperski:  
	ng serve  
4. Aplikacja została uruchomiona na domyslnym porcie (4200). Adres:  
	http://localhost:4200/  

## Budowanie aplikacji w trybie produkcyjnym 
Aplikacja budujemy poleceniem:  
	ng build --prod  
	
### Zrzuty ekranu aplikacji
#### Widok strony głównej
![zrzut 1](src/assets/images/ssc/sklep2.jpg)  
#### Widok listy produktów  
![zrzut 2](src/assets/images/ssc/sklep1.jpg)  
#### Widok szczegółowy koszyka
![zrzut 3](src/assets/images/ssc/sklep3.jpg)  
#### Widok menu edycji produktów (dla administratora)
![zrzut 4](src/assets/images/ssc/sklep4.jpg)  
#### Widok listy zamówień (dla administrator / pracownika)  
![zrzut 5](src/assets/images/ssc/sklep5.jpg)  
#### Widok szczegółowy zamówienia wraz z listą skompletowanych produktów  
![zrzut 6](src/assets/images/ssc/sklep6.jpg)  

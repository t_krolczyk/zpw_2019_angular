export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'XXXXX',
    authDomain: 'XXX',
    databaseURL: 'XXX',
    projectId: 'XXX',
    storageBucket: 'XXX',
    messagingSenderId: 'XXX'
  }
};

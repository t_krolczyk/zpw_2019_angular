import { HttpServices } from './../http.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { auth } from 'firebase';
import { Router } from '@angular/router';

import { CartService } from '../cart.service';
import { Product } from '../model/product.model';
import { Order } from './../model/order.model';
import { AuthService } from './../services/auth.service';
import { OrdersService } from './../services/orders.service';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {

  products_cart: {product: Product, qty: number}[];
  products_qty = 0;
  sum_price = 0.0;

  inputClientAddress: string;
  inputClientName: string;
  orderForm: FormGroup;

  formSent = false;
  ordID = undefined;
  isAutomated = false;

  constructor(private cart_serivce: CartService,
    fb: FormBuilder,
    private orders_service: OrdersService,
    private auth_service: AuthService,
    private http: HttpServices,
    private router: Router
  ) {

    this.orderForm = new FormGroup({
      inputClientName: new FormControl(),
      inputClientAddress: new FormControl()
    });

    this.orderForm = fb.group({
      inputClientName: ['', Validators.required],
      inputClientAddress: ['', Validators.required]
    });

  }


  ngOnInit() {
    this.registerObserver();
    this.auth_service.authState$.subscribe( usr => {
      this.http.checkUser(usr.email).subscribe( user => {
        if (user.name && user.surname) {
          this.orderForm.controls['inputClientName'].setValue(user.name + ' ' + user.surname);
        }
        if (user.address) {
          this.orderForm.controls['inputClientAddress'].setValue(user.address);
        }

        if (user.name && user.surname && user.address){
          this.isAutomated = true;
        }
      });
    });
  }

  saveOrder(): void {
    const order: Order = new Order(undefined,
      this.orderForm.controls['inputClientName'].value,
      this.orderForm.controls['inputClientAddress'].value,
      this.products_cart,
      this.sum_price,
      this.products_qty
    );

    if (this.auth_service.user) {
      order.u_email = this.auth_service.user.email;
    }

    this.cart_serivce.clear();
    this.orders_service.addOrder(order).subscribe( () => {
      this.formSent = true;
      this.routeToProducts();
    });
  }

  isNameValid(): boolean {
    return (!this.orderForm.controls['inputClientName'].touched || this.orderForm.controls['inputClientName'].valid);
  }

  isNameCorrect(): boolean {
    return this.isNameValid() && this.orderForm.controls['inputClientName'].touched;
  }

  isAddressValid(): boolean {
    return (!this.orderForm.controls['inputClientAddress'].touched || this.orderForm.controls['inputClientAddress'].valid);
  }

  isAddressCorrect(): boolean {
    return this.isAddressValid() && this.orderForm.controls['inputClientAddress'].touched;
  }

  registerObserver(): void {
    this.cart_serivce.registerObserver().subscribe(cart => {
      this.sum_price = 0;
      this.products_qty = 0;
      this.products_cart = cart;

      this.products_cart.forEach(element => {
        this.products_qty += element.qty;
        this.sum_price += element.product.unit_price * element.qty;
      });
    });
  }

  routeToProducts(): void {
    this.router.navigate(['/products']);
  }
}

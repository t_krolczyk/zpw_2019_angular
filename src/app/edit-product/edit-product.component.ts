import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../model/product.model';
import { CartService } from '../cart.service';
import { ProductsService } from '../products.service';
import { ProductCategory } from '../model/category.model';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  @Input() product: Product;
  @Input() min_price: number;
  @Input() max_price: number;

  @Output() product_remove = new EventEmitter<Product>();

  order_qty: number;
  editMode = false;
  categories: ProductCategory[];

  editName: string;
  editCategory: ProductCategory;
  editPrice: number;
  editQty: number;
  editDesc: string;
  editUrl: string;

  constructor(private cart_serivce: CartService, private product_service: ProductsService) {}

  ngOnInit() {
    this.editName = this.product.name;
    this.categories = Object.values(ProductCategory);
    this.editCategory = this.product.category;
    this.editPrice = this.product.unit_price;
    this.editQty = this.product.qty;
    this.editDesc = this.product.description;
    this.editUrl = this.product.url;
  }

  boundaryCheck(new_val: number): boolean {
    if (new_val >= 0 ) {
      return true;
    }

    return false;
  }

  incrementProductQty() {
    if (this.boundaryCheck(this.editQty + 1)) {
      this.editQty = this.editQty + 1;
      // this.product_service.addUnit(this.product, 1).subscribe();
    }
  }

  decrementProductQty() {
    if (this.boundaryCheck(this.editQty - 1)) {
      this.editQty = this.editQty - 1;
      // this.product_service.addUnit(this.product, -1).subscribe();
    }
  }

  updateProductQty(): void {
    if (this.editQty < 0) {
      this.editQty = 0;
    } else if (this.editQty > 99999) {
      this.editQty = 99999;
    }
    // this.product_service.replaceUnits(this.product, this.product.qty).subscribe();
  }

  removeProduct(product: Product) {
    this.product_remove.emit(product);
  }

  updateProduct(): void {
    this.product.name = this.editName;
    this.product.category = this.editCategory;
    this.product.unit_price = this.editPrice;
    this.product.qty = this.editQty;
    this.product.description = this.editDesc;
    this.product.url = this.editUrl;

    this.editMode = false;
    this.product_service.updateProduct(this.product).subscribe();
  }

  editProduct(product): void {
    this.editMode = this.editMode ? false : true;
  }

  takeUnit(product) {
    this.product_service.addUnit(product, -1).subscribe();
  }

  addToCart(product: Product, order_qty: number) {
    // console.log(product);
    this.order_qty = 1;
    this.cart_serivce.addProduct(product, order_qty);
    this.product_service.addUnit(product, -order_qty).subscribe();

    // this.product_service.addUnit(product, 1).subscribe();
  }
}

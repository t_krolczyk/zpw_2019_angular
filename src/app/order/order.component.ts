import { OrdersService } from './../services/orders.service';
import { Component, OnInit, Input, ElementRef, EventEmitter, Output } from '@angular/core';
import { Order } from '../model/order.model';
import { OrderStatus } from '../model/order-status.model';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  @Input() order: Order;

  @Output() editFinished = new EventEmitter<boolean>();

  editStatus = false;
  test: any;

  orderForm: FormGroup;
  checkboxGroup: FormGroup;

  constructor(private orders_services: OrdersService, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.orderForm = this.fb.group({
      checkboxGroup: this.fb.array(this.order.products_collected)
    });
  }

  enableEdit(): void {
    this.editStatus = true;
  }

  startCompleting(): void {
    this.order.status = OrderStatus.Completing;
    this.orders_services.updateOrder(this.order).subscribe( () => console.log('Zaaktualizowano!'));
    this.enableEdit();
  }

  finishOrder(): void {
    this.editStatus = false;
    this.order.status = OrderStatus.Finished;
    this.orders_services.updateOrder(this.order).subscribe( () => console.log('Zaaktualizowano!'));
    this.editFinished.emit(false);
  }

  isCompleting(): boolean {
    return this.order.status === OrderStatus.Completing ? true : false;
  }

  isWaiting(): boolean {
    return this.order.status === OrderStatus.Waiting ? true : false;
  }

  isFinished(): boolean {
    let collected = true;
    this.order.ordered_products.forEach(elem => {
      if (!this.order.products_collected.includes(elem.product._id)) {
        collected = false;
      }
    });

    return (this.order.status === OrderStatus.Completing ? true : false) && (collected);
  }

  selectAll(): void {
    this.order.products_collected = [];
    this.order.ordered_products.forEach(elem => {
        this.order.products_collected.push(elem.product._id);
    });
  }

  clearAll(): void {
    this.order.products_collected = [];
  }

  saveOrderState(): void {
    this.editStatus = false;
    this.orders_services.updateOrder(this.order).subscribe( () => console.log('Zaaktualizowano!'));
  }

  isCollected(id: string) {
    return this.order.products_collected.includes(id);
  }

  changeCollected(id: string): void {
    if (this.order.products_collected.includes(id)) {
      this.order.products_collected = this.order.products_collected.filter( arg => arg !== id);
    } else {
      this.order.products_collected.push(id);
    }
    console.log( this.order.products_collected);
  }
}

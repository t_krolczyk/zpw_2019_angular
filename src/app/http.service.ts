import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './model/product.model';
import { User } from './model/user.model';

@Injectable({
  providedIn: 'root'
})
export class HttpServices {

  private httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'}), body: '' };
  private products_api = 'http://localhost:5000/api/products';
  private user_check_api = 'http://localhost:5000/api/check_user';
  private user_register_api = 'http://localhost:5000/api/register_user';

  constructor(private http: HttpClient) { }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.products_api);
    // this.productsUpdated.next(this.products);
  }

  checkUser(email: string): Observable<User> {
    // return this.http.post<string>(this.products_api);
    // const user = new User('cas', 1);
    return this.http.post<User>(this.user_check_api, {'email': email}, this.httpOptions);
  }

  registerUser(email: string, name: string, surname: string, address: string): Observable<User> {
    return this.http.post<User>(this.user_register_api, {
      'email': email,
      'account_type': 1,
      'name': name,
      'surname': surname,
      'address': address},
      this.httpOptions
    );
  }
}

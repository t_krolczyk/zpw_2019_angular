import { ProductsService } from './../products.service';
import { CartService } from './../cart.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../model/product.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input() product: Product;
  @Input() min_price: number;
  @Input() max_price: number;

  @Output() product_remove = new EventEmitter<Product>();
  @Output() list_updated = new EventEmitter<boolean>();

  order_qty = 1;

  constructor(private cart_serivce: CartService, private product_service: ProductsService) { }

  ngOnInit() {
  }

  boundaryCheck() {
    if (!this.order_qty) {
      return;
    } else if (this.order_qty > this.product.qty) {
      this.order_qty = this.product.qty;
    } else if (this.order_qty < 1) {
      this.order_qty = 1;
    }
  }

  incrementOrderQty() {
    this.order_qty += 1;
    this.boundaryCheck();
  }

  decrementOrderQty() {
    this.order_qty -= 1;
    this.boundaryCheck();
  }

  removeProduct(product: Product) {
    this.product_remove.emit(product);
  }

  takeUnit(product) {
    this.product_service.addUnit(product, -1).subscribe();
  }

  addToCart(product: Product, order_qty: number) {
    // console.log(product);
    this.order_qty = 1;
    console.log(product);
    this.cart_serivce.addProduct(product, order_qty);
    this.product_service.addUnit(product, -order_qty).subscribe( prod => {
      this.product = prod;
      console.log(this.product);
      // this.list_updated.emit(true);
    });

    // this.product_service.addUnit(product, 1).subscribe();
  }
}

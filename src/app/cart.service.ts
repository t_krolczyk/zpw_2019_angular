import { Injectable } from '@angular/core';
import { Product } from './model/product.model';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { ProductCategory } from './model/category.model';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private products_cart: {product: Product, qty: number}[] = [];
  private products_cart_updated = new BehaviorSubject<{product: Product, qty: number}[]>([]);

  constructor() {
    // tslint:disable-next-line:prefer-const
    // let tmp: {product: Product, qty: number};
    // tmp.product = new Product(1, 'lala', ProductCategory.Pasta, 4, 12.45, 'opis', '/assets/images/kaczka.jpg');
    // tmp.qty = 3;
    // this.products_cart.push(tmp);
  }

  addProduct(product: Product, qty: number): void {
    let is_in_cart = false;

    this.products_cart.forEach( cart_prod => {
      if (cart_prod.product._id === product._id) {
        is_in_cart = true;
        cart_prod.qty += qty;
        cart_prod.product = product;
      }
    });

    if (is_in_cart === false ) {
      this.products_cart.push({product, qty});
    }
    console.log(this.products_cart);
    this.products_cart_updated.next(this.products_cart);
  }

  deleteProduct(product: {product: Product, qty: number}) {
    this.products_cart = this.products_cart.filter(p => p.product._id !== product.product._id);
    this.products_cart_updated.next(this.products_cart);
  }

  clear(): void {
    this.products_cart = [];
    this.products_cart_updated.next(this.products_cart);
  }

  registerObserver(): Observable<{product: Product, qty: number}[]> {
    return this.products_cart_updated.asObservable();
  }
}

import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Credentials } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loginMail: string;
  loginPassword: string;

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginMail = '';
    this.loginPassword = '';
  }

  logUser() {
    const registerData = {} as Credentials;
    registerData.email = this.loginMail;
    registerData.password = this.loginPassword;
    this.auth.login(registerData).then ( () => {
      this.router.navigate(['/products']);
    },
    () => {
      console.log('nie udało sięzalogować');
    }
    );
  }


}


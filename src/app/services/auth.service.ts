import { HttpServices } from './../http.service';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from 'firebase';
import { Observable } from 'rxjs/index';

export interface Credentials {
  email: string;
  password: string;
}

@Injectable({providedIn: 'root'})

export class AuthService {

  readonly authState$: Observable<User | null> = this.fireAuth.authState;
  userRole: number;

  constructor(
    private fireAuth: AngularFireAuth,
    private http: HttpServices
  ) {}

  get user(): User | null {
    return this.fireAuth.auth.currentUser;
  }

  setUserRole(role: number): void {
    this.userRole = role;
  }

  getUserRole(): number {
    return this.userRole;
  }

  login({email, password}: Credentials) {
    this.http.checkUser(email).subscribe( user => {
      if (user) {
        // console.log(user);
        this.setUserRole(user.account_type);
      }
    });
    return this.fireAuth.auth.signInWithEmailAndPassword(email, password);
  }

  register({email, password}: Credentials) {
    return this.fireAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  logout() {
    return this.fireAuth.auth.signOut();
  }
}

import { Product } from './../model/product.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Order } from './../model/order.model';
import { ProductCategory } from '../model/category.model';

const httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'}), body: '' };

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  private orders: Order[] = [];
  private ordersUpdatedGen = new BehaviorSubject<Order[]>([]);
  private orders_api = 'http://localhost:5000/api/orders';

  constructor(private http: HttpClient) {
  }

  getOrders(): Observable<Order[]> {
    // const url = this.orders_api + '/' + product._id;

    return this.http.get<Order[]>(this.orders_api);

  }

  updateOrder(order: Order): Observable<Order>  {

    const url = this.orders_api + '/' + order._id;

    return this.http.put<Order>(url, order, httpOptions);

  }

  addOrder(order: Order): Observable<Order> {
    // this.orders.push(order);
    console.log(order);
    console.log('Zamowienie zapisano!');
    // this.ordersUpdatedGen.next(this.orders);
    const url = this.orders_api;

    return this.http.post<Order>(url, order, httpOptions);
  }
}

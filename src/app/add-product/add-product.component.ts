import { ProductCategory, CategoryMap } from './../model/category.model';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Product } from '../model/product.model';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  inputName: string;
  inputCategory: ProductCategory;
  inputDescription: string;
  inputImageURL: string;
  inputPrice: number;
  inputAmount: number;

  categories: ProductCategory[];

  product: Product;
  @Output() product_add = new EventEmitter<Product>();

  constructor() {
    this.categories = Object.values(ProductCategory);
    this.inputCategory = this.categories[0];
  }

  ngOnInit() {
  }

  addProduct(form: any) {
    this.product = new Product(null,
      this.inputName,
      this.inputCategory,
      this.inputAmount,
      this.inputPrice,
      this.inputDescription,
      this.inputImageURL
    );
    console.log(this.product);
    form.reset();
    this.product_add.emit(this.product);
  }

}

import { Router } from '@angular/router';
import { CartService } from './../cart.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  logged = false;
  products_in_cart = 0;

  constructor(
    private auth: AuthService,
    private cart: CartService,
    private router: Router
  ) { }

  ngOnInit() {
    this.auth.authState$.subscribe( usr => {
      this.logged = usr ? true : false;
    });
    this.cart.registerObserver().subscribe( cart => this.products_in_cart = cart.length);
  }

  logout(): void {
    this.auth.logout();
  }

  to_cart(): void {

    this.router.navigate(['/show_cart']);
  }

}

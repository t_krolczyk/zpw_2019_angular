import { HttpServices } from './http.service';
import { Injectable } from '@angular/core';
import { Product } from './model/product.model';
import { FakeProducts } from './model/fakeproducts.model';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

const httpOptions = { headers: new HttpHeaders({'Content-Type': 'application/json'}), body: '' };

@Injectable()
export class ProductsService {
  private productsUpdated = new BehaviorSubject<Product[]>([]);
  private products_api = 'http://localhost:5000/api/products';

  constructor(private httpServices: HttpServices, private http: HttpClient) {
   }

  addUnit(product: Product, qty: number): Observable<any> {
    product.qty += qty;

    const url = this.products_api + '/' + product._id;

    return this.http.put<Product>(url, product, httpOptions);
  }

  replaceUnits(product: Product, qty: number): Observable<any> {
    product.qty = qty;
    const url = this.products_api + '/' + product._id;

    return this.http.put<Product>(url, product, httpOptions);
  }

  updateProduct(product: Product): Observable<any> {
    const url = this.products_api + '/' + product._id;

    return this.http.put<Product>(url, product, httpOptions);
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.log('Client side error: ', errorResponse.error.message);
    } else {
      console.log('Server side error: ', errorResponse.error);
    }

    return new ErrorObservable();
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.products_api);
  }

  getProduct(id: string): Observable<Product> {
    return this.http.get<Product>(this.products_api + '/' + id);
  }

  addProduct(product: Product): Observable<Product> {
    console.log(product);
    return this.http.post<Product>(this.products_api, product, httpOptions);
  }

  removeProduct(product: Product | string): Observable<any> {
    const id = typeof product === 'string' ? product : product._id;
    const url = this.products_api + '/' + id;
    console.log(url);

    return this.http.delete<any>(url, httpOptions).pipe(catchError(this.handleError));
  }
}

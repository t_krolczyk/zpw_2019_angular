import { Component, OnInit } from '@angular/core';
import { Order } from '../model/order.model';
import { OrdersService } from '../services/orders.service';
import { OrderStatus } from '../model/order-status.model';

@Component({
  selector: 'app-manage-orders',
  templateUrl: './manage-orders.component.html',
  styleUrls: ['./manage-orders.component.css']
})
export class ManageOrdersComponent implements OnInit {

  orders: Order[];

  pageQty: Number;
  showDetails = false;
  currentDetail = 0;
  possibleStatuses: OrderStatus[];
  filterStatus: OrderStatus;
  filterClient = '';

  constructor(private orders_service: OrdersService) { }

  ngOnInit() {
    this.filterStatus = OrderStatus.All;
    this.possibleStatuses = Object.values(OrderStatus);
    this.pageQty = 5;
    this.orders_service.getOrders().subscribe( orders => {
      console.log(orders);
      this.orders = orders;
    });
  }

  details(ind: number): void {
    // console.log(ind);
    // console.log(this.orders[ind]);
    this.currentDetail = ind;
    this.showDetails = true;
  }

  backToList(): void {
    this.showDetails = false;
  }

  hideDetails(event: boolean) {
    this.showDetails = event;
  }

  filterProductByOrderStatus(order: Order, selected: OrderStatus) {
    if (selected === OrderStatus.All) {
      return true;
    }

    if (order.status === selected) {
      return true;
    } else {
      return false;
    }
  }

  filterClientByName(order: Order, name_expr: string) {
    if (name_expr.length > 0) {
      return name_expr && order.client_name.includes(name_expr);
    } else {
      return true;
    }
  }
}

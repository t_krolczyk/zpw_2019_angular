import { HttpServices } from './../http.service';
import { AuthService, Credentials } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerMail: string;
  registerPassword: string;
  registerName: string;
  registerSurname: string;
  registerAddress: string;

  constructor(
    private auth: AuthService,
    private http: HttpServices,
    private router: Router
    ) { }

  ngOnInit() {
    this.registerMail = '';
    this.registerPassword = '';
    this.registerName = '';
    this.registerSurname = '';
    this.registerAddress = '';
  }

  registerUser() {
    const registerData = {} as Credentials;
    registerData.email = this.registerMail;
    registerData.password = this.registerPassword;
    this.auth.register(registerData).then( usr => {
      this.http.registerUser(usr.user.email,
        this.registerName,
        this.registerSurname,
        this.registerAddress
      ).subscribe( user => {
        this.router.navigate(['/products']);
    },
    () => {
      console.log('nie udało sięzalogować');
    }
    );
    });
  }
}

import { HttpServices } from './../http.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {

  logged = false;
  acc_type = 1;
  in_cart = 0;

  constructor(
    private auth: AuthService,
    private router: Router,
    private http: HttpServices,
    private cart: CartService
  ) {
  }

  ngOnInit() {
    this.auth.authState$.subscribe( usr => {
      this.logged = usr ? true : false;
      this.http.checkUser(this.auth.user.email).subscribe(
        user => {
        if (user) {
          this.acc_type = user.account_type;
        }
      });
    });

    this.cart.registerObserver().subscribe(  crt => this.in_cart = crt.length );
  }

  // logout(): void {
  //   this.auth.logout();
  //   // .then ( () => {
  //   //   this.router.navigate
  //   // });
  // }
}

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs'; import { AuthService } from '../services/auth.service';
import { map } from 'rxjs/operators';

@Injectable({   providedIn: 'root' })

export class AuthGuardService implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.authState$.pipe(map(log_state => {
      if (log_state !== null) { return true; }
      this.router.navigate(['/login']);

      return false;
    })
  ); }
}

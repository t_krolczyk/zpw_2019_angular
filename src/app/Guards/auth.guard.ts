import { User } from 'firebase';
import { HttpServices } from './../http.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs'; import { AuthService } from '../services/auth.service';
import { map, flatMap, mergeMap, delay } from 'rxjs/operators';
import 'rxjs/observable/EmptyObservable';
import 'rxjs/add/observable/of';

@Injectable({   providedIn: 'root' })

export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.authState$.pipe(map(log_state => {
      const requiresLogin = next.data.requiresLogin || false;
      if (log_state !== null) { return true; }
      this.router.navigate(['/login']);

      return false;
    })
  ); }
}

@Injectable({   providedIn: 'root' })
export class AuthOffGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
  ) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.authState$.pipe(map(log_state => {
      if (log_state === null) { return true; }
      this.router.navigate(['/products']);

      return false;
    })
  ); }
}

@Injectable({   providedIn: 'root' })
export class AuthAdminGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private http: HttpServices
  ) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    // console.log(next.url)

    if (this.authService.user) {
      this.http.checkUser(this.authService.user.email).subscribe(
        user => {
        if (user && user.account_type === 5) {
          this.authService.setUserRole(user.account_type);
          this.router.navigate([state.url]);
        }
      });
    }

    return this.authService.authState$.pipe(map(log_state => {
      if (log_state !== null) {
        const usr = this.authService.getUserRole();
        if (usr === 5) {
          return true;
        }
      }
      this.router.navigate(['/products']);

      return false;
    })
  );



  }
}

@Injectable({   providedIn: 'root' })
export class AuthEmployeeGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private http: HttpServices
  ) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    if (this.authService.user) {
      this.http.checkUser(this.authService.user.email).subscribe(
        user => {
        if (user && (user.account_type === 5 || user.account_type === 3)) {
          this.authService.setUserRole(user.account_type);
          this.router.navigate([state.url]);
        }
      });
    }

    return this.authService.authState$.pipe(map(log_state => {
      if (log_state !== null) {
        const usr = this.authService.getUserRole();
        if (usr === 5 || usr === 3) {
          return true;
        }
      }
      this.router.navigate(['/products']);

      return false;
    })
  );



  }
}

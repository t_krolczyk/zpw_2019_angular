import { FakeProducts } from './model/fakeproducts.model';
import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})

export class FakeserverService extends InMemoryDbService {

  constructor() {
    super();
  }

  createDb() {
    const products = new FakeProducts().products;

    return { products };
  }
}

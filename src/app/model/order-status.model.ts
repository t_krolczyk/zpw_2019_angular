export enum OrderStatus {
  All = 'Wszystkie',
  Waiting = 'Oczekujące',
  Completing = 'W realizacji',
  Finished = 'Wysłane'
}

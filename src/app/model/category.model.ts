export enum ProductCategory {
  MainDish = 'Zestawy',
  Pizza = 'Pizza',
  Burger = 'Burgery',
  Pasta = 'Makarony',
}

export interface CategoryMap<T> {
  [K: string]: T;
}

import { Product } from './product.model';
import { OrderStatus } from './order-status.model';

export class Order {
  _id: any;
  id: number;
  client_name: string;
  client_address: string;
  ordered_products: {product: Product, qty: number}[];
  order_value: number;
  products_qty: number;
  products_collected: string[];
  status: OrderStatus;
  date: Date;
  u_email?: string;

  constructor(id: number,
    client_name: string,
    client_address: string,
    ordered_products: {product: Product, qty: number}[],
    order_value = 0.0,
    products_qty = 0.0) {

    this.id = id;
    this.client_name = client_name;
    this.client_address = client_address;
    this.ordered_products = ordered_products;
    this.order_value = order_value;
    this.products_qty = products_qty;
    this.products_collected = [];
    this.status = OrderStatus.Waiting;
 }
}

import { Product } from './product.model';
import { ProductCategory } from './category.model';

export class FakeProducts {

  products: Product[] = new Array();

  constructor() {
  }
}

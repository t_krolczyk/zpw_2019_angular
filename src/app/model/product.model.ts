import { ProductCategory } from './category.model';

export class Product {
  id: number;
  _id: any;
  name: string;
  category: ProductCategory;
  qty: number;
  unit_price: number;
  description: string;
  url: string;

  constructor(id: number, name: string, category: ProductCategory, qty: number, price_unit: number, desc = '', url = '') {
    this.id = id;
    this.name = name;
    this.category = category;
    this.unit_price = price_unit;
    this.qty = qty;
    this.description = desc;
    this.url = url;
 }

}

// export interface Product {
//   id: number;
//   name: string;
//   qty: number;
//   unit_price: number;
//   description: string;
//   url: string;

// //   constructor(id: number, name: string, qty: number, price_unit: number, desc = '', url = '') {
// //     this.id = id;
// //     this.name = name;
// //     this.unit_price = price_unit;
// //     this.qty = qty;
// //     this.description = desc;
// //     this.url = url;
// //  }

// }

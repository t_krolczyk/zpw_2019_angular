export class User {
  _id: any;
  email: string;
  account_type: number;
  name: string;
  surname: string;
  address: string;

  constructor(
    email: string,
    account_type?: number,
    name?: string,
    surname?: string,
    address?: string
  ) {
    this.email = email;
    this.account_type = account_type;
    this.name = name;
    this.surname = surname;
    this.address = address;
  }
}

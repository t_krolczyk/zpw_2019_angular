import { OrderStatus } from './../model/order-status.model';
import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'callback',
    pure: false
})
export class CallbackPipe implements PipeTransform {
    transform(items: any[], mask: any, callback: (item: any, mask?: any) => boolean): any {
        if (!items || !callback) {
            return items;
        }
        return items.filter( item => callback(item, mask));
    }
}

@Pipe({
  name: 'callbackCategoryFilter',
  pure: false
})
export class CallbackCategoryFilter implements PipeTransform {
  transform(items: any[], selected: any[], callback: (item: any, selected?: any[]) => boolean): any {
      if (!items || !callback) {
          return items;
      }
      return items.filter( item => callback(item, selected));
  }
}

@Pipe({
  name: 'callbackPriceFilter',
  pure: false
})
export class CallbackPriceFilter implements PipeTransform {
  transform(items: any[], min_price: number, max_price: number, callback:
    (item: any, min_price: number, max_price: number) => boolean): any {
      if (!items || !callback) {
          return items;
      }
      return items.filter( item => callback(item, min_price, max_price));
  }
}

@Pipe({
  name: 'callbackOrderStatusFilter',
  pure: false
})
export class CallbackOrderStatusFilter implements PipeTransform {
  transform(items: any[], selected: OrderStatus, callback: (item: any, OrderStatus: OrderStatus) => boolean): any {
      if (!items || !callback) {
          return items;
      }
      return items.filter( item => callback(item, selected));
  }
}

@Pipe({
  name: 'callbackOrderClientFilter',
  pure: false
})
export class CallbackOrderClientFilter implements PipeTransform {
  transform(items: any[], mask: string, callback: (item: any, mask: string) => boolean): any {
      if (!items || !callback) {
          return items;
      }
      return items.filter( item => callback(item, mask));
  }
}

import { AuthService } from './services/auth.service';
import { HttpServices } from './http.service';
import { CartService } from './cart.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FakeserverService } from './fakeserver.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { AddProductComponent } from './add-product/add-product.component';
import { CartComponent } from './cart/cart.component';
import { ProductsService } from './products.service';
import { MainPageComponent } from './main-page/main-page.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { CallbackPipe, CallbackCategoryFilter, CallbackPriceFilter,
         CallbackOrderStatusFilter, CallbackOrderClientFilter } from './pipes/callback.pipe';
import { PurchaseComponent } from './purchase/purchase.component';
import { OrdersService } from './services/orders.service';
import { ManageOrdersComponent } from './manage-orders/manage-orders.component';
import { ManageProductsComponent } from './manage-products/manage-products.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { OrderComponent } from './order/order.component';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2';
import { environment } from 'src/environments/environment';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard, AuthOffGuard, AuthAdminGuard, AuthEmployeeGuard } from './Guards/auth.guard';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full'},
  { path: 'login',
    canActivate: [AuthOffGuard],
    component: LoginComponent,
  },
  { path: 'main',
    component: MainPageComponent,
  },
  { path: 'manage_products',
    component: ManageProductsComponent,
    data: { requiresLogin: true },
    canActivate: [AuthAdminGuard],
    children: []
  },
  {
    path: 'products',
    component: ProductsComponent
  },
  {
    path: 'register',
    canActivate: [AuthOffGuard],
    component: RegisterComponent
  },
  {
    path: 'show_cart',
    component: CartComponent
  },
  {
    path: 'purchase',
    component: PurchaseComponent
  },
  {
    path: 'logout',
    component: LogoutComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'manage_orders',
    component: ManageOrdersComponent,
    canActivate: [AuthEmployeeGuard],
  }
];



@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductComponent,
    AddProductComponent,
    CartComponent,
    MainPageComponent,
    MainMenuComponent,
    LoginComponent, LogoutComponent,
    CallbackPipe, CallbackCategoryFilter, CallbackPriceFilter, CallbackOrderStatusFilter, CallbackOrderClientFilter,
    PurchaseComponent, ManageOrdersComponent, ManageProductsComponent,
    EditProductComponent, OrderComponent, RegisterComponent, LogoutComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserModule,
    AppRoutingModule,
    AngularFireAuthModule,
    FormsModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    HttpClientModule,
    // HttpClientInMemoryWebApiModule.forRoot(FakeserverService, {delay: 500}),
    RouterModule.forRoot(
      routes
    )
  ],
  exports: [RouterModule],
  providers: [CartService, ProductsService, HttpServices, OrdersService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }

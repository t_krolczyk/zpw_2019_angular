import { Router } from '@angular/router';
import { ProductsService } from './../products.service';
import { Product } from './../model/product.model';
import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})

export class CartComponent implements OnInit {

  products_cart: {product: Product, qty: number}[];
  products_qty = 0;
  sum_price = 0.0;

  constructor(
    private cart_serivce: CartService,
    private products_service: ProductsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.registerObserver();
  }

  registerObserver(): void {
    this.cart_serivce.registerObserver().subscribe(cart => {
      this.sum_price = 0;
      this.products_qty = 0;
      this.products_cart = cart;

      this.products_cart.forEach(element => {
        this.products_qty += element.qty;
        this.sum_price += element.product.unit_price * element.qty;
      });
    });
  }

  deleteFromCart(product: {product: Product, qty: number}): void {
    // this.products_cart = this.products_cart.filter((p => p !== product));
    this.products_service.addUnit(product.product, product.qty).subscribe(() => {
      this.cart_serivce.deleteProduct(product);
    });
  }

  routeToProducts(): void {
    this.router.navigate(['/products']);
  }

  routeToPurchase(): void {
    this.router.navigate(['/purchase']);
  }
}

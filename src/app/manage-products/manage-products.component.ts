import { Component, OnInit } from '@angular/core';
import { Product } from '../model/product.model';
import { ProductsService } from '../products.service';
import { ProductCategory } from '../model/category.model';

@Component({
  selector: 'app-manage-products',
  templateUrl: './manage-products.component.html',
  styleUrls: ['./manage-products.component.css']
})
export class ManageProductsComponent implements OnInit {

  products: Product[];
  min_price: number;
  max_price: number;

  searchField: string;
  displayProductsQty: number[];
  pageQty: number;

  categories: ProductCategory[];
  selectedCategories: ProductCategory[];

  filterMinPrice: number;
  filterMaxPrice: number;

  constructor(private product_services: ProductsService) {
    this.categories = Object.values(ProductCategory);
    this.selectedCategories = [];
  }

  ngOnInit() {
    this.searchField = '';
    this.displayProductsQty = [5, 10, 25, 50, 100];
    this.pageQty = this.displayProductsQty[0];
    this.product_services.getProducts().subscribe( prdcs => this.products = prdcs);
  }

  removeProduct(product: Product) {
    this.product_services.removeProduct(product).subscribe( (deleted: Product) => {
      console.log('Produkt został usunięty z bazy danych.');
      this.getProducts();
    });

  }

  addProduct(product: Product) {
    this.product_services.addProduct(product).subscribe( (prod: Product) => {
      console.log(prod);
      this.getProducts();
    });
  }

  private calculateMinMax() {
    const tmp = this.products.map(a => a.unit_price);

    this.max_price = Math.max.apply(Math, tmp);
    this.min_price = Math.min.apply(Math, tmp);
  }

  getProducts(): void {
    this.product_services.getProducts().subscribe(products => {
      this.products = products;
      this.calculateMinMax();
    });
  }

  filterProductByName(product: Product, name_expr: string) {
    if (name_expr.length > 0) {
      return name_expr && product.name.includes(name_expr);
    } else {
      return true;
    }
  }

  filterProductByCategory(product: Product, selected_categories: ProductCategory[]) {
    if (selected_categories.length > 0) {
      return selected_categories.includes(product.category);
    } else {
      return true;
    }
  }

  filterProductByPrice(product: Product, price_min: number, price_max: number) {
    if (price_min && price_max) {
      return (price_min <= product.unit_price) && (product.unit_price <= price_max);
    } else if (price_min) {
      return (price_min <= product.unit_price);
    } else if (price_max) {
      return (product.unit_price <= price_max);
    }

    return true;
  }

  categoryClicked(category: ProductCategory) {
    if (this.selectedCategories.includes(category)) {
      const index: number = this.selectedCategories.indexOf(category);
      this.selectedCategories.splice(index, 1);
    } else {
      this.selectedCategories.push(category);
    }
  }

  onChange(new_qty: number): void {
    this.pageQty = new_qty;
  }

}

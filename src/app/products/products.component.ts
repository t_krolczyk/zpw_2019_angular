import { Product } from './../model/product.model';
import { ProductsService } from '../products.service';
import { FakeProducts } from './../model/fakeproducts.model';
import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProductCategory } from '../model/category.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})


export class ProductsComponent implements OnInit {
  products: Product[];
  min_price: number;
  max_price: number;

  searchField: string;
  displayProductsQty: number[];
  pageQty: number;

  categories: ProductCategory[];
  selectedCategories: ProductCategory[];

  filterMinPrice: number;
  filterMaxPrice: number;

  constructor(private productService: ProductsService) {
    this.categories = Object.values(ProductCategory);
    this.selectedCategories = [];
  }

  ngOnInit(): void {
    // this.registerObserver();
    this.searchField = '';
    this.displayProductsQty = [6, 12, 24];
    this.pageQty = this.displayProductsQty[0];
    this.getProducts();
    // this.productService.getProducts();
  }

  removeProduct(product: Product) {
    this.productService.removeProduct(product).subscribe( (deleted: Product) => {
      console.log('Produkt został usunięty z bazy danych.');
      this.getProducts();
    });

  }

  refresh(bool: boolean): void {
    if (bool) {
      this.getProducts();
    }
  }

  addProduct(product: Product) {
    this.productService.addProduct(product).subscribe( (prod: Product) => {
      console.log(prod);
      this.getProducts();
    });
  }

  private calculateMinMax() {
    const tmp = this.products.map(a => a.unit_price);

    this.max_price = Math.max.apply(Math, tmp);
    this.min_price = Math.min.apply(Math, tmp);
  }

  getProducts(): void {
    this.productService.getProducts().subscribe(products => {
      console.log(products);
      this.products = products;
      this.calculateMinMax();
    });
  }

  filterProductByName(product: Product, name_expr: string) {
    if (name_expr.length > 0) {
      return name_expr && product.name.includes(name_expr);
    } else {
      return true;
    }
  }

  filterProductByCategory(product: Product, selected_categories: ProductCategory[]) {
    if (selected_categories.length > 0) {
      return selected_categories.includes(product.category);
    } else {
      return true;
    }
  }

  filterProductByPrice(product: Product, price_min: number, price_max: number) {
    if (price_min && price_max) {
      return (price_min <= product.unit_price) && (product.unit_price <= price_max);
    } else if (price_min) {
      return (price_min <= product.unit_price);
    } else if (price_max) {
      return (product.unit_price <= price_max);
    }

    return true;
  }

  categoryClicked(category: ProductCategory) {
    if (this.selectedCategories.includes(category)) {
      const index: number = this.selectedCategories.indexOf(category);
      this.selectedCategories.splice(index, 1);
    } else {
      this.selectedCategories.push(category);
    }
  }

  onChange(new_qty: number): void {
    this.pageQty = new_qty;
  }
}


